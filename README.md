# auto-email-alert



## Introduction

This program is running on the VPN server. It will monitor a node's connection to VPN. If the node is offline for more than N seconds, it will send out an Email alert to a list of Email addresses. If the VPN server loses Internet connection, it will reboot the server.

## How to run?

./run.sh

