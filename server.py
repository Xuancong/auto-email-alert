#!/usr/bin/env python3
# coding=utf-8

import os, sys, time, argparse, socket, datetime, signal, traceback, pip, json
from select import select
from importlib.util import find_spec

def install(package):
    if hasattr(pip, 'main'):
        pip.main(['install', package])
    else:
        pip._internal.main(['install', package])

for pkg in ['selenium', 'seleniumwire', 'webdriver_manager']:
	if not find_spec(pkg.replace('-', '')):
		install(pkg)

from seleniumwire import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.webdriver.common.by import By

driver = None

def create_chrome():
        desired_capabilities = DesiredCapabilities.CHROME
        desired_capabilities["goog:loggingPrefs"] = {"performance": "ALL"}
        options = webdriver.ChromeOptions()
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument("start-maximized")
        options.add_argument("--autoplay-policy=no-user-gesture-required")
        options.add_argument("disable-infobars")
        options.add_argument("--disable-extensions")
        options.add_argument("--ignore-certificate-errors")
        options.add_argument("--mute-audio")
        options.add_argument("--disable-notifications")
        options.add_argument("--disable-popup-blocking")
        options.add_argument(f'user-agent={desired_capabilities}')
        driver = webdriver.Chrome(service = Service(ChromeDriverManager().install()), options = options)
        return driver

def create_firefox():
        desired_capabilities = DesiredCapabilities.FIREFOX
        desired_capabilities["goog:loggingPrefs"] = {"performance": "ALL"}
        options = FirefoxOptions()
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--headless")
        options.add_argument('--disable-dev-shm-usage')
        options.add_argument("start-maximized")
        options.add_argument("--autoplay-policy=no-user-gesture-required")
        options.add_argument("disable-infobars")
        options.add_argument("--disable-extensions")
        options.add_argument("--ignore-certificate-errors")
        options.add_argument("--mute-audio")
        options.add_argument("--disable-notifications")
        options.add_argument("--disable-popup-blocking")
        options.add_argument(f'user-agent={desired_capabilities}')
        driver = webdriver.Firefox(options=options)
        return driver


def try_close_sock(s: socket.socket):
	try:
		s.close()
	except:
		pass


all_devids = set()
msg_stack, devid2sock, devid2ip = {}, {}, {}

def cleanup():
	global S
	print(f'Closing sockets ...')
	for s in [S]+list(devid2sock.values()):
		try_close_sock(s)

isExiting = False
old_sig = signal.getsignal(signal.SIGINT)
def signal_handler(sig, frame):
	global isExiting
	isExiting = True
	cleanup()
	signal.signal(signal.SIGINT, old_sig)
	sys.exit(0)


def get_price():
	try:
		info = driver.find_elements(By.XPATH, "//td[@class='price -bold font-red-amazon']")[xpath_index].text
		return float(info.split()[-1].replace(',', ''))
	except:
		pass

	try:
		info = driver.find_elements(By.XPATH, "//p[@class='price-new lto']")[xpath_index].text
		return float(info.split()[-1].replace(',', ''))
	except:
		pass

	try:
		assert driver.find_elements(By.XPATH, "//p[@class='status']/span[@class='font-red']")[0].text.lower().strip()=='out of stock'
		return 'OUT-OF-STOCK'
	except:
		pass
	
	return None


def update_signals(driver, msg_stack):
	for ii in range(999):
		try:
			price = get_price()
			assert price is not None
			msg_stack.pop('Website refresh error', None)
			if type(price)==str:
				return price
			if price >= 9000:
				pass
				# msg_stack['BullionStar sell price has exceeded SGD9000'] = all_devids
			elif price <= 8000:
				msg_stack['BullionStar sell price has fallen below SGD8000'] = all_devids
			elif price < 8900 and price > 8100:
				[msg_stack.pop(msg) for msg in list(msg_stack.keys()) if msg.startswith('BullionStar sell price')]
			return price
		except:
			if ii >= 2:
				msg_stack['Website refresh error'] = all_devids
				return None
			driver.refresh()
			time.sleep(web_wait)


def set_tcp_keepalive(sock, after_idle_sec=100, interval_sec=10, max_fails=3):
	sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
	sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPALIVE if sys.platform == 'darwin' else socket.TCP_KEEPIDLE, after_idle_sec)
	sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, interval_sec)
	sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, max_fails)


def create_TCP(ip, port):
	S = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	if reuseaddr:
		S.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

	isFirst = True
	while True:
		try:
			S.bind((ip, port))
			break
		except:
			print(f'Waiting for Port {port} to release ..' if isFirst else '.', end = '', flush = True)
			time.sleep(5)
			isFirst = False
	S.settimeout(None)
	S.listen(max_conn)
	print(f'Listening to {ip}:{port} ...')
	return S


def send_push(msg, devid):
	global devid2sock, devid2ip
	try:
		s = devid2sock[devid]
		fp = s.makefile('rw')
		print(f'Sending message "{msg}" to {devid} ...', end = '', flush = True)
		print(msg, file = fp, flush = True)
		s.settimeout(12)
		assert msg == fp.readline().strip()
		print(f'Success')
	except Exception as e:
		print(f'Failed: {str(e)}')
		return False
	return True


def try_reply(s: socket.socket):
	try:
		fp = s.makefile('rw')
		L = fp.readline().strip()
		if L == 'get_price':
			print(json.dumps({'price': str(get_price())}), file = fp, flush = True)
			print(f'get_price from {s.getpeername()[0]}', flush = True)
		else:
			return False
	except:
		return False
	return True


if __name__ == '__main__':
	parser = argparse.ArgumentParser(usage = '$0 arg1 1>output 2>progress', description = 'what this program does',
	                                 formatter_class = argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('--port', '-p', help = 'port number', default = 8882)
	parser.add_argument('--ip', '-ip', help = 'bind interface IP address', default = '0.0.0.0')
	parser.add_argument('--max-conn', '-n', help = 'max number of connections', type = int, default = 8)
	parser.add_argument('--browser', '-b', help = 'browser driver', choices=['firefox', 'chrome'], default = 'firefox')
	parser.add_argument('--web-wait', '-t', help = 'wait time for the website', type = float, default = 2)
	parser.add_argument('--poll-interval', '-I', help = 'the interval to check website, i.e., socket accept timeout', type = float, default = 10)
	parser.add_argument('--url', '-url', help = 'the URL to monitor', default = 'https://www.bullionstar.com/buy/product/gold-bullionstar-100g')
	parser.add_argument('--xpath', '-x', help = 'the xpath to search', default = "//td[@class='amount price-sell']")
	parser.add_argument('--xpath-index', '-i', help = 'the index of the element among all elements retrieved by the xpath', default = 0, type = int)
	parser.add_argument('--reuseaddr', '-r', help = 'set SO_REUSEADDR socket option to avoid bind failure after a quick relaunch', action = 'store_true')
	parser.add_argument('--input-fifo', '-iff', help = 'input fifo for another process to send STDIN', default='')
	# nargs='?': optional positional argument; action='append': multiple instances of the arg; type=; default=
	opt = parser.parse_args()
	globals().update(vars(opt))

	driver = create_firefox() if browser=='firefox' else create_chrome()
	driver.get(url)
	time.sleep(web_wait)
	print(f'BullionStar gold sell-price is {update_signals(driver, msg_stack)}')

	S = create_TCP(ip, port)
	set_tcp_keepalive(S)
	s_fifo = os.open(input_fifo, os.O_RDONLY | os.O_NONBLOCK) if input_fifo else None
	s_fixed = [S, sys.stdin] + ([s_fifo] if s_fifo!=None else [])

	# Interruptable training session
	signal.signal(signal.SIGINT, signal_handler)

	while not isExiting:
		sock_list = s_fixed + list(devid2sock.values())
		rlist, wlist, elist = select(sock_list, [], sock_list, poll_interval)
		sock_set = set(rlist+elist)

		STDIN_LINE = input().strip() if sys.stdin in sock_set else ''
		if STDIN_LINE=='' and s_fifo in sock_set:
			STDIN_LINE = os.read(s_fifo, 1024).decode().strip()
			while os.read(s_fifo, 1024):pass
			os.close(s_fifo)
			s_fifo = os.open(input_fifo, os.O_RDONLY | os.O_NONBLOCK)
			s_fixed = [S, sys.stdin, s_fifo]

		hasClient = S in sock_set
		if hasClient:
			try:
				sock, ipa = S.accept()
			except:
				try_close_sock(S)
				devid2sock.clear()
				devid2ip.clear()
				S = create_TCP(ip, port)
				continue

		if hasClient:
			try:
				sock.settimeout(3)
				L = sock.makefile().readline()
				assert L.startswith('device_id=')
				devid = L[10:].strip()
				all_devids.add(devid)
			except:
				try_close_sock(sock)
				hasClient =False

		if hasClient:
			try:
				if devid in devid2sock:
					try_close_sock(devid2sock[devid])
				devid2sock[devid] = sock
				devid2ip[devid] = ipa
				sock.settimeout(None)
				set_tcp_keepalive(sock)
			except:
				try_close_sock(sock)

		# close invalid sockets
		for s in sock_set:
			if s not in s_fixed and not try_reply(s):
				try_close_sock(s)
				for devid in [devid for devid, sock in devid2sock.items() if s==sock]:
					devid2sock.pop(devid)
					devid2ip.pop(devid)

		# check website
		info = update_signals(driver, msg_stack)

		print(datetime.datetime.now().isoformat().split('.')[0], info, devid2ip)

		# check STDIN
		if STDIN_LINE:
			if STDIN_LINE.startswith('>>>'):
				try:
					exec(STDIN_LINE[3:].strip())
				except:
					traceback.print_exc()
			else:
				msg_stack[STDIN_LINE] = all_devids

		# push msg to devices if not yet pushed
		if msg_stack and devid2sock:
			tasks = [(msg, devid) for msg, devids in msg_stack.items() for devid in devids if devid in devid2sock]
			for msg, devid in tasks:
				if select([devid2sock[devid]], [], [], 1)[0]:
					continue
				if send_push(msg, devid):
					if msg_stack[msg] == all_devids:
						msg_stack[msg] = all_devids.copy()
					msg_stack[msg].remove(devid)
				else:
					devid2sock.pop(devid, None)
					devid2ip.pop(devid, None)

	cleanup()